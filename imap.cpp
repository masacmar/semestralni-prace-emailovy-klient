#include "imap.h"
#include <regex>

std::string fetchdata;

int write_data(char* buf, size_t size, size_t nmemb) {
    fetchdata.append((char*)buf, size*nmemb);
    //std::cout << std::string(buf, size*nmemb);
    return size*nmemb;
}
QString read_text(std::string user, std::string pass, int i, int ssl, std::string server, std::string port){
    fetchdata.clear();
    CURL *curl;
    CURLcode res = CURLE_OK;

    curl = curl_easy_init();
    if(curl) {
          curl_easy_setopt(curl, CURLOPT_USERNAME, &user[0]);
          curl_easy_setopt(curl, CURLOPT_PASSWORD, &pass[0]);
          curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
          curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

          curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

          std::string url,sec="imap";
          if(ssl>1)sec="imaps";
          url=sec+"://"+server+":"+port+"/INBOX/;uid=" + std::to_string(i) + "/;section=TEXT";
          curl_easy_setopt(curl, CURLOPT_URL, &url[0]);
          res = curl_easy_perform(curl);

          if(res != CURLE_OK){
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
                curl_easy_cleanup(curl);
                return "1";
          }

          curl_easy_cleanup(curl);
    }
    return QString::fromUtf8(fetchdata.c_str());
}

QString read_email(std::string user, std::string pass, int i, int ssl, std::string server, std::string port){
      fetchdata.clear();
      CURL *curl;
      CURLcode res = CURLE_OK;

      curl = curl_easy_init();
      if(curl) {
            /* Set username and password */
            curl_easy_setopt(curl, CURLOPT_USERNAME, &user[0]);
            curl_easy_setopt(curl, CURLOPT_PASSWORD, &pass[0]);

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

            std::string url,sec="imap";
            if(ssl==3)sec="imaps";

            url=sec+"://"+server+":"+port+"/INBOX/;uid=" + std::to_string(i) + "/;section=header.fields%20(DATE%20FROM%20SUBJECT)";
            //curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "APPEND");
            //curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

            curl_easy_setopt(curl, CURLOPT_URL, &url[0]);

            res = curl_easy_perform(curl);

            if(res != CURLE_OK){
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
                  curl_easy_cleanup(curl);
                  return "1";
            }

            /* Always cleanup */
            curl_easy_cleanup(curl);
      }
    return QString::fromUtf8(fetchdata.c_str());
}
int get_count(std::string user, std::string pass,int ssl, std::string server, std::string port){
    std::string url,sec="imap";
    if(ssl==3)sec="imaps";
    url=sec+"://"+server+":"+port;

    fetchdata.clear();
    CURL *curl;
    CURLcode res = CURLE_OK;
    curl = curl_easy_init();
    if(curl){
        curl_easy_setopt(curl, CURLOPT_USERNAME, &user[0]);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, &pass[0]);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

        curl_easy_setopt(curl, CURLOPT_URL, &url[0]);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "STATUS INBOX (UIDNEXT)");

        res = curl_easy_perform(curl);

        if(res != CURLE_OK){
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
              curl_easy_cleanup(curl);
              return 0;
        }

        curl_easy_cleanup(curl);
    }
    return std::stoi(std::regex_replace(fetchdata, std::regex(R"([\D])"), ""));
}
