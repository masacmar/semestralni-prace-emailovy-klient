#ifndef IMAP_H
#define IMAP_H
#include <curl/curl.h>
#include <string>
#include <iostream>
#include <QString>

int write_data(char* buf, size_t size, size_t nmemb);

QString read_email(std::string user, std::string pass, int i, int ssl, std::string server, std::string port);
int get_count(std::string user, std::string pass,int ssl, std::string server, std::string port);
QString read_text(std::string user, std::string pass, int i, int ssl, std::string server, std::string port);
#endif // IMAP_H

