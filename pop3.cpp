#include "pop3.h"
#include <sstream>
#include <QStringList>
#include <QDebug>

std::string fetchdata3;
int write_data3(char* buf, size_t size, size_t nmemb) {
    fetchdata3.append((char*)buf, size*nmemb);
    std::cout << std::string(buf, size*nmemb);
    return size*nmemb;
}

QString read_email3(std::string user, std::string pass, int i,int ssl, std::string server, std::string port){
    CURL *curl;
      CURLcode res = CURLE_OK;
      fetchdata3.clear();
      std::string url,sec="pop3";
      if(ssl>1)sec="pop3s";
      url=sec+"://"+server+":"+port+"/" + std::to_string(i);

      curl = curl_easy_init();
      if(curl) {
        curl_easy_setopt(curl, CURLOPT_USERNAME, &user[0]);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, &pass[0]);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data3);

        curl_easy_setopt(curl, CURLOPT_URL, &url[0]);

        //curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);


        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));

        curl_easy_cleanup(curl);
      }

      return QString::fromUtf8(fetchdata3.c_str());;
}
int get_count3(std::string user, std::string pass,int ssl, std::string server, std::string port){
    std::string url,sec="pop3";
    if(ssl==3)sec="pop3s";
    url=sec+"://"+server+":"+port;

    fetchdata3.clear();
    CURL *curl;
    CURLcode res = CURLE_OK;
    curl = curl_easy_init();
    if(curl){
        curl_easy_setopt(curl, CURLOPT_USERNAME, &user[0]);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, &pass[0]);
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, write_data3);

        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        curl_easy_setopt(curl, CURLOPT_URL, &url[0]);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "STAT");
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK){
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
              curl_easy_cleanup(curl);
              return 0;
        }

        curl_easy_cleanup(curl);
    }

    QString s;
    s=QString::fromUtf8(fetchdata3.c_str());
    s.remove(0,111);

    QStringList buf= s.split(" ");

    //buf.gcount();

    //buf>>fetchdata3;
    //buf>>fetchdata3;
    return buf.at(0).toInt();//std::stoi(fetchdata3);
}
