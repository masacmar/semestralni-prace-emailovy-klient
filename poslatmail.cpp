#include "poslatmail.h"
#include "ui_poslatmail.h"
#include "sql.h"
#include "smtp.h"


poslatmail::poslatmail(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::poslatmail)
{
    ui->setupUi(this);

}

poslatmail::~poslatmail()
{
    delete ui;
}

void poslatmail::on_odeslat_clicked()
{
    sendEmail(ui->komu->text().toStdString(),
                  current_ucet_text.toStdString(),
                  ui->komu->text().toStdString(),
                  read_where_accounts(current_ucet_text, "name").toStdString(),
                  ui->predmet->text().toStdString(),
                  ui->text->toPlainText().toStdString(),
                  read_where_accounts(current_ucet_text, "smtpserver").toStdString(),
                  read_where_accounts(current_ucet_text, "password").toStdString(),
                  read_where_accounts(current_ucet_text, "smtpport").toStdString(),
                  read_where_accounts(current_ucet_text,"readssl").toInt());
    this->close();
}

void poslatmail::on_zavrit_clicked()
{
    this->close();
}

void poslatmail::od()
{
    ui->od->setText(current_ucet_text);
}
