#ifndef POSLATMAIL_H
#define POSLATMAIL_H

#include <QDialog>

namespace Ui {
class poslatmail;
}

class poslatmail : public QDialog
{
    Q_OBJECT

public:
    explicit poslatmail(QWidget *parent = nullptr);
    ~poslatmail();
    QString current_ucet_text="";
    void od();

private slots:
    void on_odeslat_clicked();

    void on_zavrit_clicked();

private:
    Ui::poslatmail *ui;
};

#endif // POSLATMAIL_H
