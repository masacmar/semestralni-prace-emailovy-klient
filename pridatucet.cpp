#include "pridatucet.h"
#include "ui_pridatucet.h"
#include "sql.h"


pridatucet::pridatucet(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::pridatucet)
{
    ui->setupUi(this);

    ui->readssl->insertItem(1,"Žádné");
    ui->readssl->insertItem(2,"STARTTLS");
    ui->readssl->insertItem(3,"SSL/TLS");
    ui->sendssl->insertItem(1,"Žádné");
    ui->sendssl->insertItem(2,"STARTTLS");
    ui->sendssl->insertItem(3,"SSL/TLS");
}

pridatucet::~pridatucet()
{
    delete ui;
}

void pridatucet::on_ok_clicked()
{
    bool PorI=1;
    if(ui->POP3->isChecked())PorI=0;
    if(pe){add_account(ui->username->text(),ui->password->text(),ui->smtpserver->text(),ui->smtpport->text().toInt(),PorI, ui->readserver->text(), ui->readport->text().toInt(), ui->name->text(),ui->readssl->currentIndex()+1,ui->sendssl->currentIndex()+1);}
    else {edit_account(ui->username->text(),ui->password->text(),ui->smtpserver->text(),ui->smtpport->text().toInt(), PorI, ui->readserver->text(),ui->readport->text().toInt(),ui->name->text(),ui->readssl->currentIndex()+1,ui->sendssl->currentIndex()+1,current_ucet_text);}
    emit need_update();
    this->close();
}
void pridatucet::upravit(){
    ui->username->setText(read_where_accounts(current_ucet_text,"username"));
    ui->password->setText(read_where_accounts(current_ucet_text,"password"));
    ui->smtpserver->setText(read_where_accounts(current_ucet_text,"smtpserver"));
    ui->smtpport->setText(read_where_accounts(current_ucet_text,"smtpport"));
    ui->readserver->setText(read_where_accounts(current_ucet_text,"readserver"));
    ui->readport->setText(read_where_accounts(current_ucet_text,"readport"));
    ui->name->setText(read_where_accounts(current_ucet_text,"name"));
    if(read_where_accounts(current_ucet_text,"POP3orIMAP")=="1"){ui->IMAP->setChecked(1);}
    ui->readssl->setCurrentIndex(read_where_accounts(current_ucet_text,"readssl").toInt()-1);
    ui->sendssl->setCurrentIndex(read_where_accounts(current_ucet_text,"sendssl").toInt()-1);
}
