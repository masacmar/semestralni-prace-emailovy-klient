#ifndef PRIDATUCET_H
#define PRIDATUCET_H

#include <QDialog>

namespace Ui {
class pridatucet;
}

class pridatucet : public QDialog
{
    Q_OBJECT

public:
    explicit pridatucet(QWidget *parent = nullptr);
    ~pridatucet();
    void upravit();
    QString current_ucet_text="";
    bool pe=0;

private slots:
    void on_ok_clicked();
signals:
    void need_update();

private:
    Ui::pridatucet *ui;
};

#endif // PRIDATUCET_H
