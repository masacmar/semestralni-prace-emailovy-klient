QT       += core gui sql network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network printsupport

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



SOURCES += main.cpp \
    imap.cpp \
    other.cpp \
    pop3.cpp \
    poslatmail.cpp \
    pridatucet.cpp \
    smtp.cpp \
    sql.cpp \
    widget.cpp


HEADERS += smtp.h \
    imap.h \
    other.h \
    pop3.h \
    poslatmail.h \
    pridatucet.h \
    sql.h \
    widget.h

FORMS += pridatucet.ui \
    poslatmail.ui \
    widget.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target





win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'lib/' -lcurl.dll
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'lib/' -lcurl.dll
else:unix: LIBS += -L$$PWD/'lib/' -lcurl.dll

INCLUDEPATH += $$PWD/'curl/include'
DEPENDPATH += $$PWD/'curl/include'
