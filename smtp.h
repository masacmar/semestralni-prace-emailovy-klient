#ifndef SMTP_H
#define SMTP_H

#include <string>
#include <curl/curl.h>


static std::string payloadText[11];

std::string dateTimeNow();
std::string generateMessageId();

void setPayloadText(const std::string &to,
                    const std::string &from,
                    const std::string &cc,
                    const std::string &nameFrom,
                    const std::string &subject,
                    const std::string &body
                    );



struct upload_status { int lines_read; };

size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userp);
CURLcode sendEmail(const std::string &to,
                   const std::string &from,
                   const std::string &cc,
                   const std::string &nameFrom,
                   const std::string &subject,
                   const std::string &body,
                   const std::string &url,
                   const std::string &password,
                   const std::string &port,
                   const int ssl);



#endif // SMTP_H
