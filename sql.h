#ifndef SQL_H
#define SQL_H
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQuery>
#include <QListWidget>
#include <QDebug>
#include <QString>

void sql_connect();
void read_accounts(QListWidget &i);
void add_account(QString username,QString password, QString smtpserver, int smtpport, bool PorI, QString readserver, int readport, QString name,int readssl,int sendssl);
QString read_where_accounts(QString username,QString value);
void remove_account(QString user);
void edit_account(QString username,QString password, QString smtpserver, int smtpport, bool PorI, QString readserver, int readport, QString name,int readssl,int sendssl,QString acc);

#endif // SQL_H
