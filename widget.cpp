#include "widget.h"
#include "ui_widget.h"
#include "other.h"
#include "sql.h"
#include "smtp.h"
#include "imap.h"
#include "pop3.h"

int i=0;
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    sql_connect();
    read_accounts(*ui->ucty);
    /*ui->ucty->insertItem(0,"1");
    ui->ucty->insertItem(1,"2");
    ui->zpravy->insertItem(0,"flk");
    ui->zpravy->insertItem(1,"asdf");*/
    pm = new poslatmail(this);
    ac = new pridatucet(this);
    connect(ac,SIGNAL(need_update()), this, SLOT(acc_update()));
    //qDebug() << get_count3("martin@semestralka.martin","martin",0,"pop3.mail.semestralka.martin","110");
    //qDebug() << read_email3("martin@semestralka.martin","martin",1,0,"pop3.mail.semestralka.martin","110");
    //qDebug() << read_email("josef@semestralka.martin","josef",1,1,"mail.semestralka.martin","143");
}

Widget::~Widget()
{
    delete ui;
    delete pm;
    delete ac;
}



void Widget::on_pridat_clicked()
{
    ac->pe=1;
    ac->show();
}

void Widget::acc_update() {
    read_accounts(*ui->ucty);
    this->update();
}

void Widget::on_napsat_clicked()
{
    if(ui->ucty->currentRow()==-1)return;
    QString od= ui->ucty->currentItem()->text();
    if(od.isEmpty())return;
    pm->od();
    pm->show();
}

void Widget::on_ucty_itemSelectionChanged()
{
    pm->current_ucet_text = ui->ucty->currentItem()->text();
    ac->current_ucet_text = ui->ucty->currentItem()->text();
    ui->text->clear();
    ui->zpravy->clearSelection();
    ui->zpravy->clear();
    QString user=ui->ucty->currentItem()->text();
    QString pass=read_where_accounts(ui->ucty->currentItem()->text(), "password");
    QString mess="2";
    if(read_where_accounts(ui->ucty->currentItem()->text(), "POP3orIMAP")=="1"){
        int count=get_count(user.toStdString(),pass.toStdString(),read_where_accounts(user, "readssl").toInt(),read_where_accounts(user, "readserver").toStdString(),read_where_accounts(user, "readport").toStdString());
        i=1;
        if(count>20)i=count-20;
        for(;i<count;i++){
            mess=read_email(user.toStdString(),pass.toStdString(),i,read_where_accounts(user, "readssl").toInt(),read_where_accounts(user, "readserver").toStdString(),read_where_accounts(user, "readport").toStdString());
            if(mess=="1")break;
            add_mess(mess,*ui->zpravy);
        }
    } else {
        int count=get_count3(user.toStdString(),pass.toStdString(),read_where_accounts(user, "readssl").toInt(),read_where_accounts(user, "readserver").toStdString(),read_where_accounts(user, "readport").toStdString());
        i=1;
        if(count>20)i=count-20;
        for(;i<=count;i++){
            mess=read_email3(user.toStdString(),pass.toStdString(),i,read_where_accounts(user, "readssl").toInt(),read_where_accounts(user, "readserver").toStdString(),read_where_accounts(user, "readport").toStdString());
            if(mess=="1")break;
            add_mess3(mess,*ui->zpravy);
        }
    }
}

void Widget::on_zpravy_itemSelectionChanged()
{
    int index=ui->zpravy->currentRow();
    ui->text->clear();
    if(read_where_accounts(ui->ucty->currentItem()->text(), "POP3orIMAP")=="1"){ui->text->append(read_text(ui->ucty->currentItem()->text().toStdString(),read_where_accounts(ui->ucty->currentItem()->text(), "password").toStdString(),i-index-1,read_where_accounts(ui->ucty->currentItem()->text(), "readssl").toInt(),read_where_accounts(ui->ucty->currentItem()->text(), "readserver").toStdString(),read_where_accounts(ui->ucty->currentItem()->text(), "readport").toStdString()));}
    else{
        ui->text->append(pop3_text(read_email3(ui->ucty->currentItem()->text().toStdString(),read_where_accounts(ui->ucty->currentItem()->text(), "password").toStdString(),i-index-1,read_where_accounts(ui->ucty->currentItem()->text(), "readssl").toInt(),read_where_accounts(ui->ucty->currentItem()->text(), "readserver").toStdString(),read_where_accounts(ui->ucty->currentItem()->text(), "readport").toStdString())));
    }
}

void Widget::on_odstranit_clicked()
{
    if(ui->ucty->currentRow()==-1)return;
    QString current= ui->ucty->currentItem()->text();
    remove_account(current);
    read_accounts(*ui->ucty);
    this->update();

}

void Widget::on_upravit_clicked()
{
    if(ui->ucty->currentRow()==-1)return;
    ac->pe=0;
    ac->upravit();
    ac->show();

}

