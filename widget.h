#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "pridatucet.h"
#include "poslatmail.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE



class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
public slots:
    void acc_update();
private slots:
    void on_pridat_clicked();
    void on_napsat_clicked();

    void on_ucty_itemSelectionChanged();

    void on_zpravy_itemSelectionChanged();


    void on_odstranit_clicked();

    void on_upravit_clicked();

private:
    Ui::Widget *ui;
    poslatmail *pm;
    pridatucet *ac;
};
#endif // WIDGET_H


